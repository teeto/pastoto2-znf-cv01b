<?php

namespace App\Model;

use Nette\NotImplementedException;
use Nette\SmartObject;

/**
 * Class CalculatorManager
 * @package App\Model
 */
class CalculatorManager
{
	use SmartObject;

	/**
	 *
	 * @param int $x
	 * @param int $y
	 * @return int
	 */
	public function binaryAnd($x, $y)
	{
		//throw new NotImplementedException('Sčítání není implementované!'); // TODO: Doplnit implementaci sčítání.
        return $x & $y;
	}

	/**
	 *
	 * @param int $x
	 * @param int $y
	 * @return int
	 */
	public function binaryOr($x, $y)
	{
		//throw new NotImplementedException('Odčítání není implementované!'); // TODO: Doplnit implementaci odčítání.
        return $x | $y;
	}

	/**
	 *
	 * @param int $x
	 * @param int $y
	 * @return int
	 */
	public function leftShift($x, $y)
	{
		//throw new NotImplementedException('Násobení není implementované!'); // TODO: Doplnit implementaci násobení.
        for ( $i = 0; $i < $y; $i++){
            $x=$x*2;
        }
        return $x;
	}

	/**
	 *
	 * @param int $x
	 * @param int $y
	 * @return double
	 */
	public function rightShift($x, $y)
	{
		//throw new NotImplementedException('Dělení není implementované!'); // TODO: Doplnit implementaci dělení.

        $x=$x>>$y;
        return $x;
	}

    /**
     *
     * @param int $x
     * @param int $y
     * @return int
     */
    public function binaryXor($x, $y)
    {
        return $x ^ $y;
    }

    /**
     *
     * @param int $x
     * @param int $y
     * @return int
     */
    public function binaryEkv($x, $y)
    {
        $r = ~($x ^ $y);
        return $r;
    }

    /**
     *
     * @param int $x
     * @return int
     */
    public function binaryNot($x)
    {
        return ~$x;
    }

}
