<?php

namespace App\Presenters;

use App\Model\CalculatorManager;
use Nette\Application\UI\Presenter;

/**
 * Class CalculatorPresenter
 * @package App\Presenters
 */
class CalculatorPresenter extends Presenter
{
	/** @var CalculatorManager */
	private $calculatorManager;

	/**
	 * HomepagePresenter constructor.
	 * @param CalculatorManager $calculatorManager
	 */
	public function __construct(CalculatorManager $calculatorManager)
	{
		parent::__construct();
		$this->calculatorManager = $calculatorManager;
	}

	/** */
	protected function startup()
	{
		parent::startup();
		$number1 = $this->getRequest()->getParameter('number1');
		$number2 = $this->getRequest()->getParameter('number2');
		if ($this->getAction() !== 'default' && (empty($number1) || empty($number2))){
		    if($this->getAction() !== 'not' || empty($number1)){
                $this->redirect('Calculator:');
            }
        }
	}

	/** */
	public function beforeRender()
	{
		parent::beforeRender();
		$this->template->setFile(__DIR__ . "/templates/{$this->getName()}/default.latte");
	}

	/**
	 *
	 * @param int $number1
	 * @param int $number2
	 */
	public function renderAnd($number1, $number2)
	{
		$this->template->result = decbin($this->calculatorManager->binaryAnd(intval($number1, 2), intval($number2, 2)));
	}

	/**
	 *
	 * @param int $number1
	 * @param int $number2
	 */
	public function renderOr($number1, $number2)
	{
		$this->template->result = decbin($this->calculatorManager->binaryOr(intval($number1, 2), intval($number2, 2)));
	}

	/**
	 *
	 * @param int $number1
	 * @param int $number2
	 */
	public function renderLeft($number1, $number2)
	{
		$this->template->result = decbin($this->calculatorManager->leftShift(intval($number1, 2), intval($number2, 2)));
	}

	/**
	 *
	 * @param int $number1
	 * @param int $number2
	 */
	public function renderRight($number1, $number2)
	{
		$this->template->result = decbin($this->calculatorManager->rightShift(intval($number1, 2), intval($number2, 2)));
	}

    /**
     *
     * @param int $number1
     * @param int $number2
     */
    public function renderXor($number1, $number2)
    {
        $this->template->result = decbin($this->calculatorManager->binaryXor(intval($number1, 2), intval($number2, 2)));
    }

    /**
     *
     * @param int $number1
     * @param int $number2
     */
    public function renderEkv($number1, $number2)
    {
        $this->template->result = decbin($this->calculatorManager->binaryEkv(intval($number1, 2), intval($number2, 2)));
    }

    /**
     *
     * @param int $number1
     */
    public function renderNot($number1)
    {
        $this->template->result = decbin($this->calculatorManager->binaryNot(intval($number1, 2)));
    }
}
