<?php

use App\Model\CalculatorManager;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Test class for CalculatorManager
 * @testCase
 */
class CalculatorManagerTest extends TestCase
{
	/** @var CalculatorManager */
	private $calculatorManager;

	protected function setUp()
	{
		parent::setUp();
		$this->calculatorManager = new CalculatorManager();
	}

	protected function getDataForBinaryAnd()
	{
		return [
			[0b1, 0b1, 0b1],
			[0b11, 0b100, 0b0],
			[-1, 1, 1],
			[1, -1, 1],
			[0, 0, 0],
			[9, 1, 1]
		];
	}

	/**
	 * @dataProvider getDataForBinaryAnd
	 * @param int $x
	 * @param int $y
	 * @param int $result
	 */
	public function testBinaryAnd($x, $y, $result)
	{
		Assert::same($result, $this->calculatorManager->binaryAnd($x, $y));
	}

	protected function getDataForBinaryOr()
	{
		return [
			[0b1, 0b1, 0b1],
			[0b11, 0b100, 0b111],
			[-1, 1, -1],
			[1, -1, -1],
			[0, 0, 0],
			[9, 1, 9]
		];
	}

	/**
	 * @dataProvider getDataForBinaryOr
	 * @param int $x
	 * @param int $y
	 * @param int $result
	 */
	public function testBinaryOr($x, $y, $result)
	{
		Assert::same($result, $this->calculatorManager->binaryOr($x, $y));
	}

	protected function getDataForLeftShift()
	{
		return [
			[0b1, 0b1, 0b10],
			[0b11, 0b100, 0b110000],
			[-1, 1, -2],
			[0, 0, 0],
			[9, 1, 18]
		];
	}

	/**
	 * @dataProvider getDataForLeftShift
	 * @param int $x
	 * @param int $y
	 * @param int $result
	 */
	public function testLeftShift($x, $y, $result)
	{
		Assert::same($result, $this->calculatorManager->leftShift($x, $y));
	}

	protected function getDataForRightShift()
	{
		return [
			[0b1, 0b1, 0b0],
			[0b1100, 0b100, 0b0],
			[-1, 1, -1],
			[0, 0, 0],
			[9, 1, 4]
		];
	}

	/**
	 * @dataProvider getDataForRightShift
	 * @param int $x
	 * @param int $y
	 * @param int $result
	 */
	public function testRightShift($x, $y, $result)
	{
		Assert::same($result, $this->calculatorManager->rightShift($x, $y));
	}


	protected function getDataForXor()
	{
		return [
			[0b1, 0b1, 0b0],
			[0b1100, 0b100, 0b1000],
			[16, 7, 23]
		];
	}

	/**
	 * @dataProvider getDataForXor
	 * @param int $x
	 * @param int $y
	 * @param int $result
	 */
	public function testXor($x, $y, $result)
	{
		Assert::same($result, $this->calculatorManager->binaryXor($x, $y));
	}

	protected function getDataForEkv()
    	{
    		return [
    			[0b1, 0b1, -0b1],
    			[2,1,-4],
    			[0b1111,0b0000, -0b10000]
    		];
    	}

    	/**
    	 * @dataProvider getDataForEkv
    	 * @param int $x
    	 * @param int $y
    	 * @param int $result
    	 */
    	public function testEkv($x, $y, $result)
    	{
    		Assert::same($result, $this->calculatorManager->binaryEkv($x, $y));
    	}

    		protected function getDataForNot()
            	{
            		return [
            			[1, -2],
            			[0b1011100, -0b1011101],
            			[148, -149]
            		];
            	}

            	/**
            	 * @dataProvider getDataForNot
            	 * @param int $x
            	 * @param int $result
            	 */
            	public function testNot($x, $result)
            	{
            		Assert::same($result, $this->calculatorManager->binaryNot($x));
            	}
}

$testCase = new CalculatorManagerTest();
$testCase->run();
